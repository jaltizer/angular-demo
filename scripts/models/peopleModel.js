;(function () {
    'use strict';

    angular.module('Demo')
        .factory('PeopleModel', [function () {
            function PeopleModel(obj) {
                this.data = obj;
                this.FullName = function () {
                    return this.data.FirstName + ' ' + this.data.LastName;
                };
                this.Address = function () {
                    return this.data.Street + '<br/>' + this.data.City + ', ' + this.data.State;
                };
            }

            return PeopleModel;
        }]);
})();
