;(function () {
    angular.module('Demo')
        .controller('PeopleController', ['$scope', 'PeopleService', 'PeopleModel', '$location',
            function ($scope, people, ViewModel, $location) {
                $scope.viewmodel = {
                    'searchText': '',
                    'people': [],
                    'navigateToEdit': function (id) {
                        $location.path('/edit/' + id);
                    }
                };
                people.all().then(onSuccess, onError);

                function onSuccess(data) {
                    angular.forEach(data, function peopleIter(person) {
                        $scope.viewmodel.people.push(new ViewModel(person));
                    });
                    console.log($scope.viewmodel.people);
                }
                function onError() {

                }
        }]);
})();
