;(function () {
    angular.module('Demo')
        .controller('EditController', ['$scope', '$routeParams', 'PeopleService', 'PeopleModel', '$window',
            function ($scope, $params, people, ViewModel, $window) {
                $scope.viewmodel = {};
                $scope.goBack = function () {
                    $window.history.back();
                };
                $scope.save = function (data) {
                    people.save(data)
                        .then(savePersonSuccess, savePersonFailed);
                };
                $scope.iChanged = function () {
                    debugger;
                    alert('controller iChanged');
                };

                people.get(parseInt($params.id, 10))
                    .then(onGetSuccess, onGetError);

                function savePersonSuccess() {
                    $scope.goBack();
                }
                function savePersonFailed() {
                    console.log('Person save failed');
                }
                function onGetSuccess(person) {
                    $scope.viewmodel = new ViewModel(person.data);
                }
                function onGetError() {}
        }]);
})();
