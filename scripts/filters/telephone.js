;(function () {
    'use script';

    angular.module('Demo')
        .filter('telephone', function () {
            return function (value) {
                function transform(item) {
                    var link = item.replace(/([()-\s])/gi, '');
                    if (link.length < 11) {
                        link = '1' + link;
                    }

                    return '<a href="tel:+' + link.trim() + '">' + item.trim() + '</a>';
                }
                function format(item) {
                    if (item.match(/^\d+$/)) {
                        item = item.substring(0, 3) + '-' + item.substring(3, 6) + '-' + item.substring(6);
                    }
                    return item;
                }
                var text = value || '',
                    result = '';
                text = format(text);

                //RegEx Explanation
                // Group 1: matches 10 straight digits
                // Group 2: matches an initial 1 (0 or 1 times) followed by a character "- ." (0 or 1 times)
                // Group 3: matches area code, with or without () followed by a character "- ." (0 or 1 times)
                // Group 4 (main group): matches 7 straight digits or 3 digit prefix and 4 digit number with a character "- ." (0 or 1 times) in between
                // Matched styles: '1 234-567-8900' '1-234-567-8900' '1 234 567 8900' '1 (234) 567 8900' '1.234.567.8900' '(234) 567-8900'
                //                  '(134) 567 - 8900' '1234567890' '1234567'
                result = text.replaceToken(/\d{10}|(1[- \.]?){0,1}(\(?\d{3}\)?[- \.]{0,})?(\d{7}|\d{3}[- \.]{0,}\d{4})/gim, transform);
                return result;
            };
        });
})();
