;(function() {
    'use strict';

    angular.module('Demo')
        .factory('PeopleService', ['$http', '$q', '$timeout', function($http, $q, $timeout) {
            var _cache = [];

            function Service() {
                //Load the "cheap" in memory cache if it is empty
                if (_cache.length < 1) {
                    console.log('cache was empty');
                    $http.get('http://localhost:8000/scripts/api/people.json')
                        .success(function (data) {
                            _cache = data;
                        })
                        .error(function() {
                            console.warn('Get People.json FAILED');
                        });
                }
            }
            //This method allows us to replicate the process for creating a deferred object
            //  and return a promise
            function getDeferred(callback, timeout) {
                var defer = $q.defer();
                timeout = timeout || 2000;

                $timeout(function deferTimeoutDone() {
                        callback(defer);
                    }, timeout);

                return defer.promise;
            }

            function lookup(collection, id) {
                var defer = getDeferred(function lookup(dfd) {
                    for(var i = 0, len = _cache.length; i < len; ++i) {
                        var item = _cache[i];
                        if (item.Id === id) {
                            dfd.resolve({ "index": i, "data": angular.copy(item) });
                            return;
                        }
                    }
                    dfd.reject();
                }, 200);
                return defer;
            }

            Service.prototype.all = function () {
                var defer = getDeferred(function getAllPeople(dfd) {
                        dfd.resolve(_cache);
                    });

                return defer;
            };

            Service.prototype.get = function (id) {
                var defer = getDeferred(function getOnePerson(dfd) {
                        lookup(_cache, id)
                            .then(dfd.resolve, dfd.reject);
                    }, 500);

                return defer;
            };

            Service.prototype.save = function (data) {
                var defer = getDeferred(function savePerson(dfd) {
                    lookup(_cache, data.Id)
                        .then(function (found) {
                            _cache[found.index] = data;
                            dfd.resolve();
                        }, function () {
                            dfd.reject();
                        });
                }, 500);
                return defer;
            };

            return new Service();
        }]);
})();
