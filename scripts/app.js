;(function () {
    String.prototype.replaceToken = function (regEx, replacement) {
        var newText = this,
            alreadyProcessed = '',
            start = this.length,
            previous = start,
            collection = (this.match(regEx) || []).reverse();

        for(var i = 0, len = collection.length; i < len; ++i) {
            var item = collection[i];
            previous = start;
            start = newText.lastIndexOf(item, start);

            //substitue item text in newText with link
            var rep = replacement(item);
            var temp = newText.substring(start, previous);
            temp = temp.replace(item, rep);
            alreadyProcessed = newText.substring(previous, newText.length);

            //Build new string
            newText = newText.substring(0, start);
            newText += temp;
            newText += alreadyProcessed;
        }

        return newText;
    };

    angular.module('Demo', ['ngRoute'])
        .config(function ($routeProvider, $compileProvider) {
            $compileProvider.aHrefSanitizationWhitelist(/\s*(https?|tel|file):/g);
            $routeProvider
                .when('/list', {
                    templateUrl: 'views/list.html',
                    controller: 'PeopleController'
                })
                .when('/edit/:id', {
                    templateUrl: 'views/edit.html',
                    controller: 'EditController'
                })
                .otherwise({
                    redirectTo: '/list'
                });
        })
        .run(function ($rootScope) {
            $rootScope.message = 'https://groups.google.com/forum/#!forum/ql-angular-training';
        });

})();
