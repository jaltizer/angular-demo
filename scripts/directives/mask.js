;(function () {
    'use strict';

    angular.module('Demo')
        .directive('phoneMask', ['$compile', function ($compile) {
            return {
                restrict: 'AE',
                require: '^ngModel',
                scope: {
                    'max': '=',
                    'ngModel': '='
                },
                link: function (scope, elem, attrs) {
                    var template = angular.element('#template_PhoneMask');
                    scope.fieldName = attrs.name || '';
                    scope.id = attrs.id || scope.fieldName;

                    function format(item) {
                        if (item.match(/^\d+$/)) {
                            item = item.substring(0, 3) + '-' + item.substring(3, 6) + '-' + item.substring(6);
                        }
                        return item;
                    }

                    scope.changed = function () {
                        var evaluation = scope.ngModel.replace(/[^\d]/g, ''),
                            len = evaluation.length - 1;
                        if(!evaluation.substr(len, 1).match(attrs.phoneMask)) {
                            evaluation = evaluation.substring(0, len);
                        }

                        if(evaluation.length > scope.max) {
                            evaluation = evaluation.substring(0, scope.max);
                        }

                        scope.ngModel = format(evaluation);
                    };

                    elem.replaceWith($compile(template.html())(scope));
                }
            };
        }]);
})();
