;(function () {
    'use strict';

    angular.module('Demo')
        .directive('wraphtml', ['$compile', '$sce', function ($compile, $sce) {
            return {
                restrict: 'AE',
                scope: { text: '=',
                       class: '='
                       },
                link: function (scope, elem, attrs) {
                    scope.getHtml = function (text) {
                        return $sce.trustAsHtml(text || '');
                    };
                    scope.style = function () {
                        return attrs.class || '';
                    };
                    scope.href = function () {
                        if (attrs.href) {
                            return 'href=' + attrs.href;
                        }
                        return '';
                    };
                    elem.replaceWith($compile('<' + elem.context.nodeName.toLowerCase() + ' {{href()}} ng-class="style()" ng-bind-html="getHtml(text)"></' + elem.context.nodeName.toLowerCase() +'>')(scope));
                }
            };
    }]);

})();
