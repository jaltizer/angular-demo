;(function () {
    'use strict';

    angular.module('Demo')
        .directive('maxLength', ['$compile', function ($compile) {
            return {
                restrict: 'AE',
                require: '^ngModel',
                scope: {
                    'max': '=',
                    'errorMsg': '=',
                    'ngModel': '='
                },
                link: function (scope, elem, attrs) {
                    var template = angular.element('#template_MaxLength');
                    scope.fieldName = attrs.name || '';
                    scope.id = attrs.id || scope.fieldName;
                    scope.showError = false;

                    scope.changed = function () {
                        if(scope.ngModel.length > scope.max) {
                            scope.ngModel = scope.ngModel.substring(0, scope.max);
                            scope.showError = true;
                        } else {
                            scope.showError = false;
                        }
                    };
                    elem.replaceWith($compile(template.html())(scope));
                }
            };
        }]);
})();
