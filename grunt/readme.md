To start using grunt, you need to do the following

* make sure you have Node.js installed, [click here](http://nodejs.org/download/)
* install grunt cli, "npm install -g grunt-cli"
* change directories to directory holding this file
* run "npm install"
