module.exports = function(grunt) {
    var root = '../',
        scripts = root + '/scripts',
        bower = root + '/bower_components',
        host = 'localhost',
        port = 8000,
        Files = {
            angularPackage: [
                bower + '/angular/angular.js',
                bower + '/angular-bootstrap/ui-bootstrap.js',
                bower + '/angular-route/angular-route.js'
            ],
            appAngular: [
                scripts + '/providers/*.js',
                scripts + '/app.js',
                scripts + '/filters/*.js',
                scripts + '/controllers/*.js',
                scripts + '/directives/*.js',
                scripts + '/services/*.js',
                scripts + '/models/*.js'
            ]
        };

    // this is where all the grunt configs will go
    grunt.initConfig({
        // read the package.json
        // pkg will contain a reference to out pakage.json file use of which we will see later
        pkg: grunt.file.readJSON('package.json'),

        connect: {
            server: {
                options: {
                    cors: true,
                    port: port,
                    hostname: host,
                    base: root
                },
            }
        },
        watch: {
            angularPackage: {
                files: Files.angularPackage,
                tasks: ['concat:angularPackage']
            },
            appAngular: {
                files: Files.appAngular,
                tasks: ['concat:appAngular']
            }
        },
        concat: {
            options: {
                stripBanners: true,
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
                    '<%= grunt.template.today("yyyy-mm-dd") %> ' +
                '*/\n',
            },
            angularPackage: {
                src: Files.angularPackage,
                dest: root + 'scripts/angular-package.js'
            },
            appAngular: {
                src: Files.appAngular,
                dest: root + 'scripts/demo-app.js'
            }
        }
    }); // end of configuring the grunt task

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');

    var prebuildTasks = [
        'concat'
    ];

    var tasks = prebuildTasks.slice();
    tasks.push('connect:server','concat','watch');

    grunt.registerTask('default', tasks);
    grunt.registerTask('prebuild', prebuildTasks);
};
